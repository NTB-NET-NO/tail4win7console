﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Timers;
using System.ComponentModel;

namespace Tail4Win7Console
{
    class Program
    {
        static long lastLogLength = 0;
        static string Filename = "";
        static void watcher_Changed(object source, FileSystemEventArgs fsea)
        {
            // Dette er en dokumentasjon
            Console.WriteLine("File: " + fsea.FullPath + " " + fsea.ChangeType);


            FileInfo fi = new FileInfo(fsea.FullPath);

            fi.CopyTo("temp.log");

            StreamReader sr = new StreamReader("temp.log");

            Console.WriteLine(sr.ReadLine());

            sr.Close();
        }

        static void Main(string[] args)
        {

            if (args == null)
            {
                Console.Write("No args!");
            }
            else if (args.Length == 1)
            {
                // This means filename (it must)
                FileInfo fi = new FileInfo(args[0].ToString());


                if (fi.Exists == true)
                {
                    try
                    {
                        using (Stream stream = File.Open(fi.FullName,
            FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            using (StreamReader sr = new StreamReader(stream))
                            {
                                string content = "";

                                content = sr.ReadToEnd();

                                Console.Write(content);

                            }
                        }
                    }
                    catch (IOException ioe)
                    {
                        Console.Write(ioe.Message.ToString());
                    }
                }

            }
            else if (args.Length == 2)
            {


                // We have a parameter then filename
                if (args[0].ToString() == "-f")
                {

                    Filename = args[1].ToString();

                    FileInfo fi = new FileInfo(Filename);


                    lastLogLength = fi.Length;
                    System.Timers.Timer logWatcherTimer = new System.Timers.Timer();
                    logWatcherTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                    logWatcherTimer.Interval = 100;
                    logWatcherTimer.Enabled = true;

                }
            }

            Console.ReadLine();
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            // Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);


            FileInfo log = new FileInfo(Filename);

            string dir = log.DirectoryName;

            string newLines = string.Empty;

            // Just skip if log file hasn't changed
            if (lastLogLength == log.Length)
            {
                return;
            }

            if (log.Exists == true)
            {
                try
                {
                    using (Stream stream = File.Open(log.FullName,
        FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (StreamReader sr = new StreamReader(stream))
                        {
                            

                            // Set the position to the last log size and read
                            // all the content added
                            sr.BaseStream.Position = lastLogLength;

                            newLines = sr.ReadToEnd();

                        }
                    }


                    // Keep track of the previuos log length
                    lastLogLength = log.Length;

                    // Assign the result back to the worker, to be
                    // consumed by the form
                    Console.WriteLine(newLines);




                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.Message.ToString());
                }



            }
        }


    }
}
